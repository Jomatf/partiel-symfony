<?php

namespace App\Controller;

use App\Entity\Ouvrier;
use App\Form\OuvrierType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\OuvrierRepository;
use App\Repository\SpecialiteRepository;

#[Route('/ouvrier')]
class OuvrierController extends AbstractController
{
    #[Route('/', name: 'ouvrier_index')]
    public function index(OuvrierRepository $ouvriersRepository): Response
    {
        $ouvriers = $ouvriersRepository->findAll();
        return $this->render('ouvrier/index.html.twig', [
            'ouvriers' => $ouvriers,
        ]);
    }

    #[Route('/showallspecialite', name: 'specialite_show_all')]
    public function showAllSpecialite(SpecialiteRepository $specialiteRepository): Response
    {
        $specialites = $specialiteRepository->findAll();
        return $this->render('ouvrier/show_all_specialite.html.twig', [
            'specialites' => $specialites,
        ]);
    }

    #[Route('/new', name: 'ouvrier_new')]
    public function newOuvrier(Request $request, EntityManagerInterface $entityManager): Response
    {
        $ouvrier = new Ouvrier();
        $form = $this->createForm(OuvrierType::class, $ouvrier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($ouvrier);
            $entityManager->flush();

            return $this->redirectToRoute('ouvrier_index');
        }

        return $this->render('ouvrier/new_ouvrier.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
