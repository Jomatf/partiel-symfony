<?php

namespace App\Entity;

use App\Repository\ChantierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChantierRepository::class)]
class Chantier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 45)]
    private ?string $localisation = null;

    #[ORM\Column(length: 100)]
    private ?string $description = null;

    #[ORM\ManyToMany(targetEntity: Ouvrier::class, inversedBy: 'chantiers')]
    private Collection $affilier;

    public function __construct()
    {
        $this->affilier = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Ouvrier>
     */
    public function getAffilier(): Collection
    {
        return $this->affilier;
    }

    public function addAffilier(Ouvrier $affilier): self
    {
        if (!$this->affilier->contains($affilier)) {
            $this->affilier->add($affilier);
        }

        return $this;
    }

    public function removeAffilier(Ouvrier $affilier): self
    {
        $this->affilier->removeElement($affilier);

        return $this;
    }
}
