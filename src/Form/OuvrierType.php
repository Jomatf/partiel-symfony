<?php

namespace App\Form;

use App\Entity\Ouvrier;
use App\Entity\Specialite;
use Doctrine\Common\Collections\Expr\Value;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class OuvrierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom',TextType::class, [
                'label' => 'Nom : ',
            ])
            ->add('prenom',TextType::class, [
                'label' => 'Prenom : ',
            ])
            // ->add('specialite',ChoiceType::class, [
            //     'label' => 'Specialite : ',
                 
            //     // 'attr' => [
            //     //     'class' => 'form-control',
            //     // ],
            // ])
            ->add('specialite', EntityType::class, ['class'=>Specialite::class])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ouvrier::class,
        ]);
    }
}
